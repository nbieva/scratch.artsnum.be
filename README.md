#Scratch 

> Source: [Floss Manuals](https://fr.flossmanuals.net/initiation-a-scratch/quest-ce-que-scratch/)

Scratch est un logiciel de programmation visuelle destiné principalement aux enfants, mais aussi aux adolescents, où ils peuvent créer leurs propres histoires interactives, des jeux et des animations - et partager leurs créations avec d'autres tout autour du monde. Dans le processus de conception et de programmation des projets de Scratch, les jeunes apprennent à penser de façon créative, à raisonner systématiquement, et travailler en collaboration.

Cela se révèle donc être un outil très ludique et pédagogique. 

Scratch est spécialement conçu pour les enfants de 8 à 16 ans, mais il est utilisé par des personnes de tous les âges. Des millions de gens créent des projets avec Scratch dans un large éventail de situations, y compris les maisons, les écoles, les musées, les bibliothèques et les centres communautaires.

![blocs](/assets/interface-s.png)

Contrairement à certains outils de création de jeux ou d'animation, Scratch permet de concevoir toutes les interactions possibles, ce n'est pas un outil préformaté.

Coder avec Scratch est beaucoup plus simple que coder avec un vrai langage de programmation traditionnel, puisque la création de scripts est réalisée à partir d'instructions simple se présentant sous la forme d'un assemblage de blocs (contrôles, variables, capteurs...).

Dans le même esprit, il y a quelques années, l'expérimentation sur le langage LOGO (langage simplifié de programmation) était introduit dans les écoles.

![Logo](/assets/logo-angle-droit.jpg)

Scratch se présente en ligne sous la forme d'une application et site web interactifs, et hors-ligne sous la forme d'un logiciel ayant la même interface.


Vous trouverez de plus amples informations sur les différents blocs sur [cette page](http://maths-gp-2015.e-monsite.com/pages/algorithmique-et-programmation/scrach/notions-de-base-scratch.html)

![Peter](/assets/peter.jpg)