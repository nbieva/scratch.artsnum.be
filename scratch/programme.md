#Qu'est-ce qu'un programme?

Tu connais déjà un grand nombre de programmes qui ne sont pas informatiques et ne concernent pas directement les ordinateurs...
Songe à un programme comme **une suite d'instructions à exécuter dans un ordre bien précis et selon certaines conditions**.

Tu exécutes toi-même de petits programmes le matin, quand tu t'habilles.. (Si il pleut, mettre mon K-Way. Sinon, mettre mes lunettes de soleil...)

![Recette](/assets/slides-recette-notext.jpg)

Une recette de cuisine est un programme, une suite d'instructions à suivre scrupuleusement et exécuter dans un ordre précis. Si la recette de demande de mettre du sel sans en indiquer la quantité, tes invités risquent d'avoir une petite surprise... ;)

![Textile](/assets/slides-textile-notext.jpg)

Les motifs que tu trouves sur tes vêtements suivent aussi des suites d'instructions très précises. D'ailleurs, la première machine programmable était un métier à tisser, en 1801! Les "programmes" étaient faits de petits trous dans des cartes perforées, comme on utilise dans un orgue de barbarie...
Demande à tes grands-parents de te montrer ce qu'ils ont utilisé pour te tricoter ton pull préféré quand tu était petit ;)

![Musique](/assets/slides-musique-notext.jpg)

Une partition de musique indique aussi comment interpréter un morceau, quand retourner au début, etc.

![Peinture](/assets/slides-artistes-notext.jpg)

Certains artistes, dans leurs oeuvres, exécutent certains *programmes*, des instructions qui leur sont propres. Certains les font même exécuter par d'autres personnes! ;)

Que ce soit pour la cuisine, la musique, ou la construction d'un LEGO, **l'ordre des instructions est très important**. Le résultat ne serait pas le même si tu modifiais l'ordre d'exécution du programme!

![Aller au travail](/assets/allerautravail.png)

Retiens que, d'une certaine manière, l'ordinateur ne fait qu'exécuter des instructions précises, pour lesquelles il est programmé. Il n'est ni très inventif par lui-même, ni magique. Par contre, **c'est un outil extraordinaire au service de TA créativité et de TON imaginaire**!

L'idée de code est aussi partout dans la nature. Pense aux coquillages, au tournesols, ou à ton ADN!

![Nature](/assets/slides-nature-notext.jpg)