#Ressources

Cette page reprend une série de ressources ou de liens intéressants qui te seront utiles dans ton parcours avec Scratch. Si tu en trouves d'autres que tu aimerais partager, n'hésites pas à nous le faire savoir!


##Pour Scratch

+ https://scratch.mit.edu/
+ http://www.reseau-canope.fr/atelier-yvelines/spip.php?article1158
+ [Scratch dans les Floss Manuals](https://fr.flossmanuals.net/initiation-a-scratch/les-differents-elements-de-linterface/)
+ http://co-dev.org/scratch-jeu-attrape-mangue/
+ http://co-dev.org/scratch-le-tutoriel/
+ http://scratchfr.free.fr/Scratchfr_v2014/Scratch_Cards_v2.0frA4_January27th.pdf
+ [Atelier Scratch avec Micode](https://www.youtube.com/playlist?list=PLCDSDEC6wOhfZBjutgcB-bWmeQvsmwgD9)
+ (https://www.youtube.com/watch?v=YmchOz_Xg5w) à créer un jeu complet.

##Pour la programmation et le code en général

+ https://studio.code.org/courses
+ https://pixees.fr/classcode-v2/
+ https://code.org/minecraft


##Autres outils
+ https://ide.makeblock.com/#/
+ http://www.mblock.cc

https://www.youtube.com/watch?v=hvvme-WMzQM