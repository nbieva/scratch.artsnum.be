#Ressources

Cette page reprend une série de ressources ou de liens intéressants qui te seront utiles dans ton parcours avec Scratch. Si tu en trouves d'autres que tu aimerais partager, n'hésites pas à nous le faire savoir!


##Pour Scratch

+ https://scratch.mit.edu/
+ https://code.org/minecraft
+ http://www.reseau-canope.fr/atelier-yvelines/spip.php?article1158


##Pour la programmation et le code en général

+ https://studio.code.org/courses
+ https://pixees.fr/classcode-v2/


##Autres outils
+ https://ide.makeblock.com/#/
+ http://www.mblock.cc


##Divers
+ (Jacques Bertin)

+ **Micode** (France Télévisions) t'apprends en [8x 3 minutes](https://www.youtube.com/watch?v=YmchOz_Xg5w) à créer un jeu complet.
+ http://co-dev.org/scratch-jeu-attrape-mangue/
+ http://co-dev.org/scratch-le-tutoriel/
+ http://scratchfr.free.fr/Scratchfr_v2014/Scratch_Cards_v2.0frA4_January27th.pdf