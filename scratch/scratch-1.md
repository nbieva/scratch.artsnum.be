#Pour commencer...

Nos objectifs pour cette première étape sont:

+ **Comprendre ce qu'est un programme**
+ Se familiariser avec l'**interface**[^1] de Scratch
+ Créer un **premier projet**
+ Le **partager**

![Interface](https://www.epslemont.ch/images/RESSOURCES/OCOM_MITIC/SCRATCH/IMAGES/01_interface.png)
(Image: https://www.epslemont.ch/)

##Le jeu d'attrape

Une bonne façon de commencer à travailler avec Scratch est de créer un jeu. Le jeu d'attrape est un chouette jeu que tu pourras mettre en oeuvre assez facilement (Pommes qui tombent d'un arbre, un requin qui doit manger des petits poissons...). 

Tu trouveras les différentes instructions dans [ce document](/assets/attrape.pdf).

Pour ce projet, comme pour tous les autres d'ailleurs, il te faudra comprendre la façon dont sont placés les objets dans ta scène. Pour placer tes lutins, tu vas devoir utiliser ce système de coordonnées:

![Les coordonnées](/assets/slides-coordonnees.jpg)

Heureusement, Scratch t'aide un petit peu en indiquant en bas à droite de ta scène les coordonnées de ta souris. C'est déjà ça! ;)

##Pour aller plus loin...

+ Tu peux créer plusieurs niveaux dans ton jeu. Par exemple, lorsque le score atteint la valeur 20, un plus grand nombre d'objets pourraient tomber. Ou tomber plus vite. L'arrière plan et l'univers visuel peuvent changer également. Tu peux créer ainsi 2 ou 3 niveaux différents qui compliquent le jeu.
+ Tu peux créer et dessiner ton propre lutin, et aller chercher l'arrière-plan qui te convient sur le web, et le personnaliser. Voici une bonne adresse pour modifier une image en ligne: [https://www.photopea.com/](https://www.photopea.com/)
+ Tu peux également créer un *attrapeur* un peu plus "actif", qui tire des flèches sur les objets qui tombent par exemple.
+ Ajoute un décompte ou un chronomètre
+ Etc.

Commence peut-être par te familiariser avec ce que tu vas réaliser et les différentes étapes du projet. Tu peux **prendre quelques notes sur une feuille de papier**. Cela t'aidera beaucoup!

Des cartes sont disponibles pour t'aider, mais ce sera peut-être plus facile pour toi d'utiliser directement les conseils dans l'interface de Scratch! Clique sur le menu "conseils" dans le haut de ton écran, puis suit les instructions qui apparaissent à droite de ton écran...

Certaines photos de cartes sont également disponibles [ici](https://photos.app.goo.gl/SQd69ZbThmadyj4R7).

![Conseils](/assets/conseils.jpg)

Tu peux aussi créer:

+ Jeu de **cache-cache**
+ Jeu de **Pong**
+ Un **Labyrinthe**

![Jeux](/assets/jeux.jpg)

> Une fois ton jeu réalisé, demande à ton voisin de jouer avec! (et écoute ses commentaires! Ils seront très précieux!)

##Partage ton projet!

Partager son travail avec les autres est quelque chose que font les plus grands programmeurs. C'est une notion essentielle! Quand tu as terminé, partage ton projet ou donne-nous ton fichier si tu le désires. Nous le partagerons avec le reste du groupe.

![Jeux](/assets/partage.png)

#Ressources supplémentaires

Va faire un petit tour sur notre page [Bonnes adresses](ressources.md)! Tu y trouveras de l'aide quand tu es bloqué, ou de super idées pour de nouveaux projets!


[^1]: **Interface**: C'est ce que tu as devant les yeux. On parle aussi d'interface utilisateur. C'est l'ensemble des fenêtres, boutons et menus d'un programme. Concrètement, c'est ce qu'il y a entre toi et le code du programme.