#Progression

+ Les coordonnées
+ Les entrées et les sorties (pour capteurs plus tard)
+ Le principe des blocs
+ Les lutins (créer, modifier)
+ Les costumes
+ Les arrière-plans (créer, modifier)
+ Les boucles
+ Déplacer un lutin
+ Déplacer un lutin à l'aide du clavier
+ Rebondir sur le bord de la scène
+ Dire "Hello"
+ Montrer/masquer
+ Créer une variable
+ Savoir choisir un lutin/arrière-plan sur le web
+ Créer son propre lutin/arrière-plan
+ Créer son propre bloc (sa propre fonction)