#Pour commencer...

> Sois attentive ou attentif! Nous avons mis sur cette page une série de liens intéressants pour avancer dans ton travail. Sur Internet, souvent, quand un mot est bleu, un lien se cache derrière. Il faut cliquer dessus pour le découvrir...

-----

Nos objectifs pour cette première étape sont:

+ Comprendre ce qu'est un programme
+ Se familiariser avec l'interface[^1] de Scratch
+ Démarrer un premier projet

Pour commencer à travailler avec Scratch, nous avons identifié pour toi 2 idées qui sont un bon point de départ. Choisis celle qui t'inspire le plus! ;) 

+ **Réalise un jeu**
+ **Imagine une histoire**

Commence peut-être par te familiariser avec ce que tu vas réaliser et les différentes étapes du projet. Tu peux **prendre quelques notes sur une feuille de papier**. Cela t'aidera beaucoup!

Un Dojo ne suffira peut-être pas pour terminer ton premier projet. Ce n'est pas grave! On a tout le temps! Above all, be cool! (Avant tout, sois cool!)




##Réalise un jeu

Pour ce qui est des jeux, voici quelques propositions:

+ Jeu de **cache-cache**
+ Jeu de **Pong**
+ Jeu d'**attrape** (Pommes qui tombent d'un arbre, un requin qui doit manger des petits poissons...)
+ **Labyrinthe**

![Jeux](/assets/jeux.jpg)

Pour aller plus loin: 
+ Crée des lutins ou des arrière-plans personnalisés
+ Ajoute un score à ton jeu
+ Ajoute un décompte ou un chronomètre

Une fois ton jeu réalisé, essaie de:

+ L'adapter, le personnaliser. Crée différents niveaux ou difficultés. Crée des lutins personnalisés..
+ Faire jouer les autres à ton jeu (et écoute leurs commentaires! Ils seront très précieux!)

##Imagine une histoire

Pour ton histoire, n'oublie pas les éléments suivants : 

+ Les changements d'**arrière-plans**, de **lutins** et de **costumes**  (Importation/Création d'éléments)
+ Les **dialogues** (dire, penser..)
+ Le **son**

Une fois le projet réalisé, essaie de:

+ Personnaliser ton histoire (si ce n'est pas déjà le cas). Invente tes propres lutins..
+ Raconte ton histoire aux autres (et écoute leurs commentaires! Ils seront très précieux!)

##Partage ton projet!

Partager son travail avec les autres est quelque chose que font les plus grands programmeurs. C'est une notion essentielle! Quand tu as terminé, partage ton projet ou donne-nous ton fichier si tu le désires. Nous le partagerons avec le reste du groupe.

![Jeux](/assets/partage.png)

#Ressources supplémentaires

Va faire un petit tour sur notre page [Bonnes adresses](ressources.md)! Tu y trouveras de l'aide quand tu es bloqué, ou de super idées pour de nouveaux projets!



[^1]: **Interface**: C'est ce que tu as devant les yeux. On parle aussi d'interface utilisateur. C'est l'ensemble des fenêtres, boutons et menus d'un programme. Concrètement, c'est ce qu'il y a entre toi et le code du programme.
