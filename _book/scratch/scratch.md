#Scratch 

L'environnement de Scratch

+ Interface
+ Scène
+ Arrière-plans
+ Lutins
+ Scripts
+ Blocs


Vous trouverez de plus amples informations sur les différents blocs sur [cette page](http://maths-gp-2015.e-monsite.com/pages/algorithmique-et-programmation/scrach/notions-de-base-scratch.html)